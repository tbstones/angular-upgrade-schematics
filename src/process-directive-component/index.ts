import { strings } from "@angular-devkit/core";
import { classify } from "@angular-devkit/core/src/utils/strings";
import {
  apply,
  mergeWith,
  Rule,
  SchematicContext,
  template,
  Tree,
  url
} from "@angular-devkit/schematics";
import {
  ArrayLiteralExpression,
  FunctionExpression,
  NodeArray,
  SourceFile,
  Statement,
  SyntaxKind
} from "typescript";
import { getScopeBindings } from "../lib/bindings";
import { getDirectiveDependencies } from "../lib/di";
import { getSourceFile } from "../lib/get-source-file";
import { getDirectiveInjectionToken } from "../lib/injection-tokens";
import { getLifeCycleEvents } from "../lib/lifeCycle";
import {
  formatMethodNames,
  getClassMethodsFromClass,
  getClassMethodsFromConstFunction,

  getClassMethodsFromPropertiesClass, getClassMethodsFromScopeFunction
} from "../lib/methods";
import { ComponentTemplateData } from "../lib/model";
import {
  getClassDeclarationNode,
  getControllerNode,
  getFirstFunctionDeclarationNode
} from "../lib/nodes";
import { getFlattenedSourceNodes } from "../lib/nodes/get-flattened-source-nodes";
import {
  getClassPropertiesFromFunction,
  getFunctionScopePropertiesFromFunction as getScopePropertiesFromFunction
} from "../lib/property";

interface ProcessComponentOptions {
  path: string;
}
export function processDirectiveComponent(
  options: ProcessComponentOptions
): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    const path = options.path;
    const ast = getSourceFile(path);
    const flattenedAst = getFlattenedSourceNodes(ast);
    const templatePath = "../../src/files/component";
    const token = getDirectiveInjectionToken(flattenedAst);

    const className = token.replace(/^rma/i,'');
    
    const templateData: ComponentTemplateData = {
      bindings: getScopeBindings(flattenedAst),
      classMethods: [],
      className: classify(`${className} Component`),
      classProperties: [],
      constructorStatements: [],
      dependencies: getDirectiveDependencies(flattenedAst),
      fileName: className,
      lifeCycle: getLifeCycleEvents(ast),
      originalFile: `${__dirname}/${path}`,
      selector: token,
    };

    const classDeclaration = getClassDeclarationNode(flattenedAst);
    const isClass = typeof classDeclaration !== "undefined";

    const controllerNode = getControllerNode(flattenedAst);

    if (isClass) {
      const methods = getClassMethodsFromClass(classDeclaration, ast);
      const propertyMethods = getClassMethodsFromPropertiesClass(classDeclaration, ast)

      templateData.classMethods = formatMethodNames([...methods, ...propertyMethods]);
    } else if (
      controllerNode?.initializer.kind === SyntaxKind.ArrayLiteralExpression
    ) {
      const elements = (controllerNode.initializer as ArrayLiteralExpression)
        .elements;

      const controller: FunctionExpression | undefined = elements.find(
        (el) => el.kind === SyntaxKind.FunctionExpression
      ) as FunctionExpression;

      templateData.classProperties = getClassProperties(
        controller.body.statements,
        ast
      );
      templateData.classMethods = getClassMethods(
        controller.body.statements,
        ast
      );
    } else {
      const statements = getFirstFunctionDeclarationNode(flattenedAst)?.body
        ?.statements;

      templateData.classProperties = getClassProperties(statements, ast);
      templateData.classMethods = getClassMethods(statements, ast);
    }

    const templateSource = apply(url(templatePath), [
      template({
        ...strings,
        ...templateData,
      }),
    ]);

    return mergeWith(templateSource)(tree, _context);
  };
}

function getClassMethods(
  statements: NodeArray<Statement> | undefined,
  ast: SourceFile
) {
  const scopedMethods = getClassMethodsFromScopeFunction(statements, ast);
  const constMethods = getClassMethodsFromConstFunction(statements, ast);
  return formatMethodNames([...scopedMethods, ...constMethods]);
}

function getClassProperties(
  statements: undefined | NodeArray<Statement>,
  ast: SourceFile
) {
  const funcProps = getClassPropertiesFromFunction(statements, ast);
  const scopeProps = getScopePropertiesFromFunction(statements, ast);
  return [...scopeProps, ...funcProps];
}
