// This file was migrated from <%= originalFile %>
import { Directive,<% if(bindings.inputs.length) {%> Input,<%}%> <% if(bindings.outputs.length) {%> Output, EventEmitter,<%}%><% if(lifeCycle.length){%> <%=  lifeCycle.join(', ') %> <%}%> } from '@angular/core';

@Directive({
  selector: '[<%= camelize( selector ) %>]',
})
export class <%= classify(className) %> <% if(lifeCycle.length){%> implements <%= lifeCycle.join(', ') %> <%}%> {
<% if(bindings.inputs.length ) {%><% for(let input of bindings.inputs) { %>
    @Input()
    public <%= input.name %>: void;
    <%}%><% } %>
<% if(bindings.outputs.length) {%><% for(let output of bindings.outputs) { %>
    @Output()
    public <%= output.name %>: EventEmitter<void> = new EventEmitter();
    <%}%><% } %>

<% for(let property of classProperties) { %><% if(property.initialiser) {%>
     private <%= property.name %>= <%= property.initialiser %>;
      <%} else{ %>
      private <%= property.name %>: void;<% } %><%}%>

<% if (dependencies || constructorStatements) { %>
  public constructor(<% for (let dependency of dependencies ) { %>
            private readonly <%= dependency[0] %>: <%= dependency[1] %>,<% } %>
    ) {
      <% for (let statement of constructorStatements ) { %>
<%= statement %><% }%>
    }
<% } %>


<% for( let method of classMethods) { %>
  private <%= method.name %>(<% for( let param of method.params) {%> <%= param %> :void,<% } %> ):void {
      <%= method.body.join('\n') %> 
  }
<% } %>

}
