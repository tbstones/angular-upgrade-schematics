// This file was migrated from <%= originalFile %>
import { Injectable } from '@angular/core';
                        
@Injectable({
  providedIn: 'root',
})
export class <%= classify(className) %> {

  <% for(let property of classProperties) { %>
    <% if(property.initialiser) {%>
     private <%= property.name %>= <%= property.initialiser %>;
      <%} else{ %>
      private <%= property.name %>: void;
      <% } %>
  <%}%>

<% if (dependencies || constructorStatements) { %>
  public constructor(<% for (let dependency of dependencies ) { %>
            private readonly <%= dependency[0] %>: <%= dependency[1] %>,<% } %>
    ) {
      <% for (let statement of constructorStatements ) { %>
      <%= statement %><% }%>
    }
<% } %>


<% for( let method of classMethods) { %>
  private <%= method.name %>(<% for( let param of method.params) {%> <%= param %> :void,<% } %> ):void {
    <%= method.body.join('\n') %> 
  }
<% } %>

}
