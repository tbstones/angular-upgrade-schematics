import { strings } from "@angular-devkit/core";
import { classify } from "@angular-devkit/core/src/utils/strings";
import {
  apply,
  mergeWith,
  Rule,
  SchematicContext,
  template,
  Tree,
  url
} from "@angular-devkit/schematics";
import { getBindings } from "../lib/bindings";
import { getClassProperties, getConstructorStatements } from "../lib/constructor";
import { getComponentDependencies } from "../lib/di";
import { getSourceFile } from "../lib/get-source-file";
import { getComponentInjectionToken } from "../lib/injection-tokens";
import { getLifeCycleEvents } from "../lib/lifeCycle";
import {
  formatMethodNames,
  getClassMethodsFromClass,
  getClassMethodsFromConstFunction,

  getClassMethodsFromPropertiesClass, getClassMethodsFromScopeFunction
} from "../lib/methods";
import { ComponentTemplateData } from "../lib/model";
import {
  getClassDeclarationNode,
  getFirstConstructorNode,
  getFirstFunctionDeclarationNode
} from "../lib/nodes";
import { getFlattenedSourceNodes } from "../lib/nodes/get-flattened-source-nodes";
import {
  getClassPropertiesFromFunction,
  getFunctionScopePropertiesFromFunction as getScopePropertiesFromFunction
} from "../lib/property";

interface ProcessServiceOptions {
  path: string;
}
export function processComponent(options: ProcessServiceOptions): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    const path = options.path;
    const ast = getSourceFile(path);

    const flattenedAst = getFlattenedSourceNodes(ast);
    const templatePath = "../../src/files/component";
    const token = getComponentInjectionToken(flattenedAst);

    const className = token.replace(/^rma/i,'');

    console.log(classify(className));
    const templateData: ComponentTemplateData = {
      bindings: getBindings(flattenedAst),
      classMethods: [],
      className: classify(`${className} Component`),
      classProperties: [],
      constructorStatements: [],
      dependencies: getComponentDependencies(flattenedAst),
      fileName: className,
      lifeCycle: getLifeCycleEvents(ast),
      originalFile: `${__dirname}/${path}`,
      selector: token
    };

    const classDeclaration = getClassDeclarationNode(flattenedAst);
    const isClass = typeof classDeclaration !== "undefined";

    if (isClass) {
      const methods = getClassMethodsFromClass(classDeclaration, ast);
      const propertyMethods = getClassMethodsFromPropertiesClass(classDeclaration, ast)

      const constructorBody = getFirstConstructorNode(flattenedAst)?.body;

      if (constructorBody?.statements) {
        const statements = constructorBody.statements;
        templateData.classProperties = getClassProperties(statements);
        templateData.constructorStatements = getConstructorStatements(
          statements,
          ast
        );
      }
      
      templateData.classMethods = formatMethodNames([...methods, ...propertyMethods]);
    } else {
      const statements = getFirstFunctionDeclarationNode(flattenedAst)?.body
        ?.statements;

      const funcProps = getClassPropertiesFromFunction(statements, ast);
      const scopeProps = getScopePropertiesFromFunction(statements, ast);

      const scopedMethods = getClassMethodsFromScopeFunction(statements, ast);
      const constMethods = getClassMethodsFromConstFunction(statements, ast);

      templateData.classProperties = [...scopeProps, ...funcProps];

      templateData.classMethods = formatMethodNames([
        ...scopedMethods,
        ...constMethods,
      ]);
    }

    const templateSource = apply(url(templatePath), [
      template({
        ...strings,
        ...templateData,
      }),
    ]);

    return mergeWith(templateSource)(tree, _context);
  };
}
