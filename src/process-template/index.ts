import { Rule, SchematicContext, Tree } from "@angular-devkit/schematics";
import { convertTemplate, saveTemplate } from "../lib/template";

export function processTemplate(_options: any): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    const { path } = _options;
    const content: Buffer | null = tree.read(path);

    let baseTemplate = (content) ? content.toString() : '';

    baseTemplate = convertTemplate(baseTemplate);

    saveTemplate(path, tree, baseTemplate);
    return tree;
  };
}

