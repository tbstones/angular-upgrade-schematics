import { Rule, SchematicContext, Tree } from "@angular-devkit/schematics";
import { getSourceFile } from "../lib/get-source-file";
import { AstNode } from "../lib/model/ast-node.model";
import { getFlattenedSourceNodes } from "../lib/nodes/get-flattened-source-nodes";
import { AngularTypes } from "../lib/model/angular-types.model";
import { getPropertyAccessExpressionNodes } from "../lib/nodes";

export function processDirectory(_options: any): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    const path = `./app`;

    tree.getDir(path).visit((filePath) => {
      if (!filePath.endsWith(".js")) {
        return;
      }

      const ast = getSourceFile(`${process.cwd()}${filePath}`);

      const flattened = getFlattenedSourceNodes(ast);

      const angularFileType = getAngularFileType(flattened);

      switch (angularFileType) {
        case AngularTypes.Controller:
          console.log("Controller");
          break;
        case AngularTypes.Service:
          console.log("Service");
          break;
        default:
          console.log("This type is not handled");
      }
    });

    return tree;
  };
}

function getAngularFileType(flattenedAst: AstNode[]): AngularTypes {
  const types: string[] = Object.values(AngularTypes);
  const node = getPropertyAccessExpressionNodes(flattenedAst).find(
    (n) => n.name?.escapedText && types.includes(n.name.escapedText as string)
  );

  if (!node) {
    throw new Error("Node not found to get type");
  }

  return node.name?.escapedText as AngularTypes;
}
