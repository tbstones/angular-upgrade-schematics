import { strings } from "@angular-devkit/core";
import { classify } from "@angular-devkit/core/src/utils/strings";
import {
  apply,
  mergeWith,
  Rule,
  SchematicContext,
  template,
  Tree,
  url
} from "@angular-devkit/schematics";
import {
  getClassProperties,
  getConstructorStatements
} from "../lib/constructor";
import { getServiceDependencies } from "../lib/di";
import { getSourceFile } from "../lib/get-source-file";
import { getServiceInjectionToken } from "../lib/injection-tokens/get-service-injection-token";
import { getClassMethodsFromClass, getClassMethodsFromPropertiesClass, getClassMethodsFromScopeFunction } from "../lib/methods";
import { ServiceTemplateData } from "../lib/model";
import {
  getClassDeclarationNode,
  getFirstConstructorNode,
  getFirstFunctionDeclarationNode
} from "../lib/nodes";
import { getFlattenedSourceNodes } from "../lib/nodes/get-flattened-source-nodes";
import { getClassPropertiesFromFunction } from "../lib/property";

interface ProcessServiceOptions {
  path: string;
}
export function processServices(options: ProcessServiceOptions): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    const path = options.path;
    const ast = getSourceFile(path);
    const flattenedAst = getFlattenedSourceNodes(ast);
    const templatePath = '../../src/files/services';

    const token = getServiceInjectionToken(flattenedAst);

    const stripedToken = token.replace(/service/i, '');

    const templateData: ServiceTemplateData = {
      originalFile: `${__dirname}/${path}`,
      constructorStatements: [],
      classMethods: [],
      classProperties: [],
      className: classify(`${stripedToken} Service`),
      fileName: stripedToken,
      dependencies: getServiceDependencies(flattenedAst)
    };

    const constructorBody = getFirstConstructorNode(flattenedAst)?.body;

    if (constructorBody?.statements) {
      const statements = constructorBody.statements;
      templateData.classProperties = getClassProperties(statements);
      templateData.constructorStatements = getConstructorStatements(
        statements,
        ast
      );
    }

    const classDeclaration = getClassDeclarationNode(flattenedAst);
    const isClass = typeof classDeclaration !== "undefined";

    if (isClass) {
      const methods = getClassMethodsFromClass(classDeclaration, ast);
      const propertyMethods = getClassMethodsFromPropertiesClass(classDeclaration, ast)

      templateData.classMethods = [...methods, ...propertyMethods];
    } else {
      const statements = getFirstFunctionDeclarationNode(flattenedAst)?.body
        ?.statements;

      templateData.classProperties = getClassPropertiesFromFunction(
        statements,
        ast
      );
      templateData.classMethods = getClassMethodsFromScopeFunction(statements, ast);
    }

    const templateSource = apply(url(templatePath), [
      template({
        ...strings,
        ...templateData,
      }),
    ]);

    return mergeWith(templateSource)(tree, _context);
  };
}
