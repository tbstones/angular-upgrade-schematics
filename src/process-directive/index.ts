import { strings } from "@angular-devkit/core";
import { classify } from "@angular-devkit/core/src/utils/strings";
import {
  apply,
  mergeWith,
  Rule,
  SchematicContext,
  template,
  Tree,
  url
} from "@angular-devkit/schematics";
import { getBindings } from "../lib/bindings";
import { getSourceFile } from "../lib/get-source-file";
import { getDirectiveInjectionToken } from "../lib/injection-tokens";
import { LifeCycleEvents } from "../lib/lifeCycle/model";
import { ComponentTemplateData } from "../lib/model";
import { getFlattenedSourceNodes } from "../lib/nodes/get-flattened-source-nodes";

interface ProcessComponentOptions {
  path: string;
}
export function processDirective(
  options: ProcessComponentOptions
): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    const path = options.path;
    const ast = getSourceFile(path);
    const flattenedAst = getFlattenedSourceNodes(ast);
    const templatePath = "../../src/files/directive";
    const token = getDirectiveInjectionToken(flattenedAst);

    const templateData: ComponentTemplateData = {
      bindings: getBindings(flattenedAst),
      classMethods: [{ name: 'ngOnInit', params: [], body: [] }],
      className: classify(`${token} Directive`),
      classProperties: [],
      constructorStatements: [],
      dependencies: [],
      fileName: token,
      lifeCycle: [LifeCycleEvents.OnInit],
      originalFile: `${__dirname}/${path}`,
      selector: token
    };

    const templateSource = apply(url(templatePath), [
      template({
        ...strings,
        ...templateData,
      }),
    ]);

    return mergeWith(templateSource)(tree, _context);
  };
}
