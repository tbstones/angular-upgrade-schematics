import {
  ArrowFunction,
  BinaryExpression,
  Block,
  ClassDeclaration,
  ExpressionStatement,
  FunctionDeclaration,
  FunctionExpression,
  isArrowFunction,
  isExpressionStatement,
  isFunctionDeclaration,
  isFunctionExpression,
  MethodDeclaration,
  NodeArray,
  PropertyAccessExpression,



  PropertyDeclaration, SourceFile,
  Statement,
  SyntaxKind
} from "typescript";
import { ClassVariable } from "../model";
import { printNode } from "../utils";

export function getClassMethodsFromConstFunction(
  statements: NodeArray<Statement> | undefined,
  ast: SourceFile
): ClassVariable[] {
  return !statements
    ? []
    : statements
      .filter((statment: Statement) => isFunctionDeclaration(statment))
      .map((fn: FunctionDeclaration) => {
        return {
          name: fn.name?.getText() ?? 'unknownName',
          params: fn.parameters.map((param) => param.name.getText()),
          body: (fn.body as Block)!.statements.map((s) => replaceCode(s, ast)),
        };
      });
}

export function getClassMethodsFromScopeFunction(
  statements: NodeArray<Statement> | undefined,
  ast: SourceFile
): ClassVariable[] {
  return !statements
    ? []
    : statements
      .filter((statment: Statement) => isExpressionStatement(statment))
      .map((statement: ExpressionStatement) => statement.expression)
      .filter(
        (expression: BinaryExpression) =>
          expression.right &&
          (isArrowFunction(expression.right) ||
            isFunctionExpression(expression.right))
      )
      .map((expression: BinaryExpression) => {
        const left = expression.left as PropertyAccessExpression;
        const right = expression.right as ArrowFunction | FunctionExpression;

        const statements = (right.body as Block)?.statements ?? [];

        return {
          name: left.name.getText(),
          params: right.parameters.map((param) => param.name.getText()),
          body: statements.map((s) => replaceCode(s, ast)),
        };
      });
}

export function replaceCode(s: Statement, ast: SourceFile): string {
  return printNode(s, ast)
    .replace(/(\$scope|\$ctrl|self)\./g, "this.")
    .replace(/\$analytics.pageTrack\((.*)\)/ig, '$analytics.pageTrack.next($1)');
}

export function getClassMethodsFromClass(
  classDeclaration: ClassDeclaration | undefined,
  ast: SourceFile
): ClassVariable[] {
  return !classDeclaration
    ? []
    : classDeclaration.members
      .filter((e) => e.kind === SyntaxKind.MethodDeclaration)
      .map((method: MethodDeclaration) => ({
        name: method.name.getText(),
        params: method.parameters.map((param) => param.name.getText()),
        body: method.body?.statements.map((s) => replaceCode(s, ast)
        ),
      }));
}

export function getClassMethodsFromPropertiesClass(
  classDeclaration: ClassDeclaration | undefined,
  ast: SourceFile
): ClassVariable[] {
  return !classDeclaration
    ? []
    : classDeclaration.members
      .filter((e) => e.kind === SyntaxKind.PropertyDeclaration)
      .filter((d: PropertyDeclaration) => d.initializer && isArrowFunction(d.initializer))
      .map((method: PropertyDeclaration) => ({
        name: method.name.getText(),
        params: (method.initializer as ArrowFunction).parameters.map((param) => param.name.getText()),
        body: ((method.initializer as ArrowFunction).body as Block).statements.map((s) => replaceCode(s, ast)
        ),
      }));
}