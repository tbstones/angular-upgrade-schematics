import { ClassVariable } from "../model";

export function formatMethodNames(methods: ClassVariable[]): ClassVariable[] {
  return methods.map((method) => {
    if (method.name === "$onInit") {
      method.name = "ngOnInit";
    }
    
    if (method.name === "$onDestroy") {
      method.name = "ngOnDestroy";
    }
    
    if (method.name === "$onChanges") {
      method.name = "ngOnChanges";
    }

    return method;
  });
}