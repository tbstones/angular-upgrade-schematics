import { SourceFile } from "typescript";
import { LifeCycleEvents } from "./model";

export function getLifeCycleEvents(ast: SourceFile): string[] {
  const events = [];

  if (/\$onInit/.test(ast.text)) {
    events.push(LifeCycleEvents.OnInit);
  }

  if (/\$onChanges/.test(ast.text)) {
    events.push(LifeCycleEvents.OnChanges);
  }

  if (/\$onDestroy/.test(ast.text)) {
    events.push(LifeCycleEvents.OnDestroy);
  }

  return events;
}
