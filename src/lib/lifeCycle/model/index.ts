export enum LifeCycleEvents{
 OnInit = 'OnInit',
 OnChanges = 'OnChanges',
 OnDestroy = 'OnDestroy',
}