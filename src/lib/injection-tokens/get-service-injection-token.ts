import { PropertyAccessExpression } from "typescript";
import { AngularTypes } from "../model/angular-types.model";
import { AstNode } from "../model/ast-node.model";
import { getFirstNodeByName, getPropertyAccessExpressionNodes } from "../nodes";
import { getInjectionToken } from "./get-injection-token";

export function getServiceInjectionToken(flattened: AstNode[]): string {

  const serviceNode = getFirstNodeByName<PropertyAccessExpression>(
    getPropertyAccessExpressionNodes(flattened),
    AngularTypes.Service
  )

  return serviceNode ? getInjectionToken(serviceNode)
    : getInjectionToken(getFirstNodeByName(
      getPropertyAccessExpressionNodes(flattened),
      AngularTypes.Factory
    ));
}
