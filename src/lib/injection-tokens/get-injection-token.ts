import { ObjectLiteralExpression, SyntaxKind } from "typescript";
import { AstNode } from "../model/ast-node.model";

export function getInjectionToken(node?: AstNode): string {
  if (!node) {
    throw new Error("No statement was passed to getInjectionToken. Maybe you used the wrong converter?");
  }

  const stringNode = (node as any)?.parent.arguments.find(
    (n: any) => n.kind === SyntaxKind.StringLiteral
  );

  if (stringNode) {
    return stringNode.text;
  }

  const objectNode: ObjectLiteralExpression = (node as any)?.parent.arguments.find(
    (n: any) => n.kind === SyntaxKind.ObjectLiteralExpression
  );

  if (objectNode) {
    return objectNode.properties[0]!.name!.getText();
  }
 
  throw new Error('Token name could not be found')
}
