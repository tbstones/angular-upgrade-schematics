import { AngularTypes } from "../model/angular-types.model";
import { AstNode } from "../model/ast-node.model";
import { getFirstNodeByName, getPropertyAccessExpressionNodes } from "../nodes";
import { getInjectionToken } from "./get-injection-token";

export function getDirectiveInjectionToken(flattened: AstNode[]): string {
  return getInjectionToken(
    getFirstNodeByName(
      getPropertyAccessExpressionNodes(flattened),
      AngularTypes.Directive
    )
  );
}
