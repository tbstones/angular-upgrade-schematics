import { getInjectionToken } from "./get-injection-token";
import { AstNode } from "../model/ast-node.model";
import { AngularTypes } from "../model/angular-types.model";
import { getFirstNodeByName, getPropertyAccessExpressionNodes } from "../nodes";

export function getControllerInjectionToken(flattened: AstNode[]) {
  return getInjectionToken(
    getFirstNodeByName(
      getPropertyAccessExpressionNodes(flattened),
      AngularTypes.Controller
    )
  );
}