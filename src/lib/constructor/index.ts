import {
  BinaryExpression,
  ExpressionStatement,
  NodeArray,
  PropertyAccessExpression,
  SourceFile,
  Statement,
  SyntaxKind
} from "typescript";
import { printNode } from "../utils";

export function getConstructorStatements(
  statements: NodeArray<Statement>,
  ast: SourceFile
) {
  return statements
    .map((statement: ExpressionStatement) => printNode(statement, ast))
    .filter((m) => m !== "'ngInject';");
}

export function getClassProperties(statements: NodeArray<Statement>) {
  return statements
    .map(
      (node: ExpressionStatement) =>
        (node.expression as BinaryExpression)!.left as PropertyAccessExpression
    )
    .filter(
      (statement) => statement?.expression.kind === SyntaxKind.ThisKeyword
    )
    .map((statement) => ({ name: statement.name.text }));
}
