import {
  Identifier,
  ObjectLiteralExpression,
  PropertyAssignment
} from "typescript";
import { getScopeBindingsNode } from "../nodes";

export function getInputsFromScope(flattenedAst: any[]): string[] {
  const scopeNode = getScopeBindingsNode(flattenedAst);

  if (!(scopeNode as any)?.initializer?.properties) {
    return [];
  }

  return (scopeNode?.initializer as ObjectLiteralExpression).properties.map(
    (a: PropertyAssignment) => (a.name as Identifier).escapedText as string
  );
}
