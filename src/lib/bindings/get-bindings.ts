import {
  Identifier,
  ObjectLiteralExpression,
  PropertyAssignment
} from "typescript";
import { AstNode } from "../model/ast-node.model";
import { getBindingsNode, getScopeBindingsNode } from "../nodes";
import { Binding, Bindings, BindingType } from "./model";

// This is a copy of the file below, please combine into a single method
export function getScopeBindings(flattenedAst: AstNode[]): Bindings {
  const bindingsNode = getScopeBindingsNode(flattenedAst);

  if (!(bindingsNode as any)?.initializer?.properties) {
    return { inputs: [], outputs: [] };
  }

  const bindings = (bindingsNode?.initializer as ObjectLiteralExpression).properties.map(
    getBinding
  );

  const inputs = bindings.filter(
    (binding) => binding.type === BindingType.Input
  );
  const outputs = bindings.filter(
    (binding) => binding.type === BindingType.Output
  );
  const twoWay = bindings.filter(
    (binding) => binding.type === BindingType.Banana
  );

  const twoWayInputs = twoWay.map((binding) => ({
    ...binding,
    type: BindingType.Input,
  }));

  const twoWayOutput = twoWay.map((binding) => ({
    ...binding,
    name: `${binding.name}Change`,
    type: BindingType.Output,
  }));

  return {
    inputs: [...inputs, ...twoWayInputs],
    outputs: [...outputs, ...twoWayOutput],
  };
}

export function getBindings(flattenedAst: AstNode[]): Bindings {
  const bindingsNode = getBindingsNode(flattenedAst);

  if (!(bindingsNode as any)?.initializer?.properties) {
    return { inputs: [], outputs: [] };
  }

  const bindings = (bindingsNode?.initializer as ObjectLiteralExpression).properties.map(
    getBinding
  );

  const inputs = bindings.filter(
    (binding) => binding.type === BindingType.Input
  );
  const outputs = bindings.filter(
    (binding) => binding.type === BindingType.Output
  );
  const twoWay = bindings.filter(
    (binding) => binding.type === BindingType.Banana
  );

  const twoWayInputs = twoWay.map((binding) => ({
    ...binding,
    type: BindingType.Input,
  }));

  const twoWayOutput = twoWay.map((binding) => ({
    ...binding,
    name: `${binding.name}Change`,
    type: BindingType.Output,
  }));

  return {
    inputs: [...inputs, ...twoWayInputs],
    outputs: [...outputs, ...twoWayOutput],
  };
}



export function getBinding(prop: PropertyAssignment): Binding {
  return {
    name: getName(prop),
    optional: isOptional(prop),
    type: getType(prop),
  };
}

function getName(prop: PropertyAssignment): string {
  return (prop.name as Identifier).getText();
}

function isOptional(prop: PropertyAssignment): boolean {
  return /\?/.test(prop.initializer.getText());
}

function getType(prop: PropertyAssignment): BindingType {
  const value = prop.initializer.getText();

  if (/=/.test(value)) {
    return BindingType.Banana;
  }

  if (/&/.test(value)) {
    return BindingType.Output;
  }

  return BindingType.Input;
}
