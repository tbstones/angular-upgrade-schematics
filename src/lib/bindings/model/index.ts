export interface Binding {
  name: string;
  optional: boolean;
  type: BindingType
}

export interface Bindings{
  inputs: Binding[],
  outputs: Binding[],
}

export enum BindingType {
  Banana = "both",
  Input = 'input',
  Output = "output",
}