import { dasherize } from "@angular-devkit/core/src/utils/strings";
import { Tree } from "@angular-devkit/schematics";

export function saveTemplate(path: string, tree: Tree, strContent: string) {
    const outputPath = dasherize(path.replace(/^rma/i,'')).replace(/\.html/, '.component.html');

    if (tree.exists(outputPath)) {
        tree.overwrite(outputPath, strContent);
    }
    else {
        tree.create(outputPath, strContent);
    }
}