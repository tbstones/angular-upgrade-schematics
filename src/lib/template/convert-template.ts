
import cheerio from 'cheerio';
import pretty from 'js-beautify';

export function convertTemplate(content: string): string {
    const $ = cheerio.load(content, {
        normalizeWhitespace: false,
        decodeEntities: false,
    });

    const ngIf =
        // Add ng-containers
        $('[ng-if]').each((_i, el) => {
            const $el = $(el);
            const ifVal = $el.attr('ng-if');
            $el.removeAttr('ng-if');
            $el.wrap($(`<ng-container ng-if="${ifVal}"></ng-container>`));
        });

    content = $('body').html() ?? '';

    content = content
        .replace(/(::|::!)?(\$ctrl|ctrl|vm)(.styles)?./g, '')
        .replace(/(ng-| )disabled="(.*)"/g, '[disabled]="$2"')
        .replace(/(ng-| )checked="(.*)"/g, '[checked]="$2"')
        .replace(/(ng-| )required="(.*)"/g, '[required]="$2"')
        .replace(/(ng-| )pattern="(.*)"/g, '[pattern]="$2"')
        .replace(/ng-if=/g, '*ngIf=')
        .replace(/ng-repeat="(\w+)\sin\s([^"]*)"/g, '*ngFor="let $1 of $2"')
        .replace(/ng-show="(.*)"/g, '*ngIf="$1"')
        .replace(/ng-hide="(.*)"/g, '*ngIf="!($1)"')
        .replace(/([^\w])track by (.*)"/g, '$1;trackBy:$2"')
        .replace(/ng-style=/g, '[ngStyle]=')
        .replace(/(ng-src|src|alt)="{{(\S+)}}"/g, '[$1]="$2"')
        .replace(/\[ng-src]=/g, '[src]=')
        .replace(/ng-class=/g, '[ngClass]=')
        .replace(/([^\w])ng-model([^\w])/g, '$1[(ngModel)]$2')
        .replace(/([^\w])ng-value([^\w])/g, '$1[value]$2')
        .replace(/\[ngClass]="(\w+)"/g, 'class="$1"')
        .replace(/ng-bind-html/g, '[innerHTML]')
        .replace(/ng-click=/g, '(click)=')
        .replace(/ng-dblclick/g, '(dblclick)')
        .replace(/ng-blur/g, '(blur)')
        .replace(/ng-focus/g, '(focus)')
        .replace(/ng-mouseenter/g, '(mouseenter)')
        .replace(/ng-mouseleave/g, '(mouseleave)')
        .replace(/ng-mouseover/g, '(mouseover)')
        .replace(/ng-keypress/g, '(keypress)')
        .replace(/ng-keydown/g, '(keydown)')
        .replace(/ng-keyup/g, '(keyup)')
        .replace(/ng-change/g, '(change)')
        .replace(/ng-change/g, '(change)')
        .replace(/ng-submit/g, '(ngSubmit)')

        // switch
        .replace(/([^\w])ng-switch-when([^\w])/g, '$1*ngSwitchCase$2')
        .replace(/([^\w])ng-switch-default([^\w])/g, '$1*ngSwitchDefault$2')
        .replace(/([^\w])ng-switch([^\w])/g, '$1[ngSwitch]$2')
        //Analytics
        .replace(/analytics-on/g, 'angulartics2On="click"')
        .replace(/analytics-label/g, 'angularticsLabel')
        .replace(/analytics-category/g, 'angularticsCategory')
        .replace(/analytics-event/g, 'angularticsAction')
        ;

    return pretty.html(content, {
        unformatted: ['code', 'pre', 'em', 'strong'],
        indent_inner_html: true,
        indent_char: ' ',
        indent_size: 4,
        wrap_attributes: 'force-aligned',
    });
}