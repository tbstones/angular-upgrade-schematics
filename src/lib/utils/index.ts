export * from './printer';

export function getEnumKeyByEnumValue(subjectEnum: any, value: string | number) {
  let keys = Object.keys(subjectEnum).filter((x) => subjectEnum[x] === value);
  return keys.length > 0 ? keys[0] : null;
}
