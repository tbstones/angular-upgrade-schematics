import { createPrinter, EmitHint, NewLineKind, SourceFile } from "typescript";

export function printNode(node: any, ast: SourceFile): string {
  const printer = createPrinter({
    newLine: NewLineKind.LineFeed,
  });

  return printer.printNode(EmitHint.Unspecified, node, ast);
}