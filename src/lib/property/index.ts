import {
  BinaryExpression,
  ExpressionStatement,
  isArrowFunction,
  isExpressionStatement,
  isFunctionExpression,
  NodeArray,
  PropertyAccessExpression,
  SourceFile,
  Statement,
  SyntaxKind,
  VariableDeclaration,
  VariableStatement
} from "typescript";
import { ClassVariable } from "../model";
import { printNode } from "../utils";

export function getClassPropertiesFromFunction(
  statements: undefined | NodeArray<Statement>,
  ast: SourceFile
): ClassVariable[] {
  return !statements
    ? []
    : statements
        .filter((e: Statement) => e.kind === SyntaxKind.VariableStatement)
        .reduce(
          (accum: ClassVariable[], statement: VariableStatement) =>
            accum.concat(
              statement.declarationList.declarations.map(
                (variable: VariableDeclaration): ClassVariable => ({
                  name: variable.name.getText(),
                  initialiser: variable.initializer 
                    ? printNode(variable.initializer, ast).replace(
                    /\$scope\./g,
                    "this."
                  ) 
                  : '',
                })
              )
            ),
          []
        );
}
export function getFunctionScopePropertiesFromFunction(
  statements: NodeArray<Statement> | undefined,
  ast: SourceFile
): ClassVariable[] {
  return !statements
    ? []
    : statements
        .filter((statment: Statement) => isExpressionStatement(statment))
        .map((statement: ExpressionStatement) => statement.expression)
        .filter(
          (expression: BinaryExpression) =>
            expression.right &&
            !(
              isArrowFunction(expression?.right) ||
              isFunctionExpression(expression?.right)
            )
        )
        .map((expression: BinaryExpression) => {
          const left = expression.left as PropertyAccessExpression;
          const right = expression.right as any;
          return {
            name: left.name.getText(),
            initialiser: printNode(right, ast).replace(/\$scope\./g, "this."),
          };
        });
}
