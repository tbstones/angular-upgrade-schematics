import {
  ArrayLiteralExpression,
  BinaryExpression,
  CallExpression,
  Expression,
  FunctionExpression,
  NodeArray,
  PropertyAccessExpression,
  StringLiteral,
  SyntaxKind
} from "typescript";
import { AngularTypes } from "../model/angular-types.model";
import { AstNode } from "../model/ast-node.model";
import {
  get$InjectNode,
  getControllerNode,
  getFirstConstructorNode,
  getFirstFunctionDeclarationNode,
  getFirstNodeByName,
  getPropertyAccessExpressionNodes
} from "../nodes";

export type InjectionPropsToTokens = string[][];

export function getServiceDependencies(
  astNodes: AstNode[]
): InjectionPropsToTokens {
  return getDependenices(astNodes, AngularTypes.Service);
}

export function getDirectiveDependencies(
  astNodes: AstNode[]
): InjectionPropsToTokens {
  return getDependenices(astNodes, AngularTypes.Directive);
}

export function getComponentDependencies(
  astNodes: AstNode[]
): InjectionPropsToTokens {
  return getDependenices(astNodes, AngularTypes.Component);
}

function getDependenices(
  astNodes: AstNode[],
  type: AngularTypes
): InjectionPropsToTokens {

  if (getInlineDeps(astNodes, type)) {
    console.log('Getting inline dependencies')
    return getInlineDepTokens(astNodes, type);
  }

  if (get$InjectNode(astNodes)) {
    console.log('Getting $inject dependencies')
    return getInjectedParams(astNodes);
  }

  if (getFirstConstructorNode(astNodes)) {
    console.log('Getting constructor dependencies')
    return getNgInjectParameters(astNodes);
  }

  if (getControllerNode(astNodes)) {
    console.log('Getting controller dependencies')
    return getControllerInjectParams(astNodes);
  }

  return [];
}

function getControllerInjectParams(
  astNodes: AstNode[]
): InjectionPropsToTokens {
  const node = getControllerNode(astNodes);

  if (!node) {
    return [];
  }

  const elements = (node.initializer as ArrayLiteralExpression).elements;

  const tokens = elements.filter((el) => el.kind === SyntaxKind.StringLiteral);

  const controller: FunctionExpression | undefined = elements.find(
    (el) => el.kind === SyntaxKind.FunctionExpression
  ) as FunctionExpression;

  if (controller === undefined) {
    return [];
  }

  return mapInjectionPropsToTokens(controller.parameters, tokens);
}

function getNgInjectParameters(astNodes: AstNode[]): InjectionPropsToTokens {
  const parameters = getFirstConstructorNode(
    astNodes
  )!.parameters.map((param) => param.name.getText());

  return filterInjectionProperties(parameters, parameters);
}

function getInjectedParams(astNodes: AstNode[]): InjectionPropsToTokens {
  const node = get$InjectNode(astNodes);

  const typesNodes = (node!.parent as BinaryExpression)
    .right as ArrayLiteralExpression;

  const constructorNodes =
    getFirstConstructorNode(astNodes) ??
    getFirstFunctionDeclarationNode(astNodes);

  return mapInjectionPropsToTokens(
    constructorNodes!.parameters,
    typesNodes.elements
  );
}

function getInlineDepTokens(
  astNodes: AstNode[],
  type: AngularTypes
): InjectionPropsToTokens {
  const inlineDeps = getInlineDeps(astNodes, type);
  const parameters = getFirstFunctionDeclarationNode(astNodes)!.parameters;

  if (!inlineDeps) {
    return [];
  }

  return mapInjectionPropsToTokens(parameters, inlineDeps.elements);
}

function mapInjectionPropsToTokens(
  props: NodeArray<any>,
  typeNodes: NodeArray<Expression> | Expression[]
): InjectionPropsToTokens {
  const paramNames = props.map((prop) => prop.name.escapedText);

  const injectTokenNames = typeNodes
    .filter(
      (expression: Expression) => expression.kind === SyntaxKind.StringLiteral
    )
    .map((element: StringLiteral) => element.text);

  return filterInjectionProperties(paramNames, injectTokenNames);
}

function filterInjectionProperties(
  params: string[],
  types: string[]
): InjectionPropsToTokens {
  const ignore = [
    "$interval",
    "$location",
    "$q",
    "$rootScope",
    "$state",
    "$scope",
    "$timeout",
    "$sce",
    "$uibModal",
    "$window",
  ];

  //this needs to be better
  const editedTypes = types
    .map((type) => type === 'FeatureProviderService' ? 'FeaturesService' : type)
    .map((type) => type === '$translate' ? 'TranslateService' : type)
    .map((type) => type === 'rmaHttpService' ? 'RmaHttpClient' : type)
    .map((type) => type === '$analytics' ? 'Angulartics2' : type);

  return params
    .map((val: string, index: number) => [val, editedTypes[index]])
    .filter((dep) => !ignore.includes(dep[1]));
}

export function getInlineDeps(
  astNodes: AstNode[],
  type: AngularTypes
): ArrayLiteralExpression | undefined {
  let node = getAngularTypeNode(astNodes, type);

  //This is horrible and should be changed but we are in hackathon
  if(type=== AngularTypes.Service && !node){
    node = getAngularTypeNode(astNodes, AngularTypes.Factory);
  }

  if (!node) {
    return undefined;
  }

  return (node.parent as CallExpression).arguments.find(
    (n: Expression) => n.kind === SyntaxKind.ArrayLiteralExpression
  ) as ArrayLiteralExpression;
}

function getAngularTypeNode(astNodes: AstNode[], type: AngularTypes) {
  return getFirstNodeByName<PropertyAccessExpression>(
    getPropertyAccessExpressionNodes(astNodes),
    type
  );
}
