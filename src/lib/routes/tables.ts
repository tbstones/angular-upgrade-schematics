import Table from "cli-table3";
import { AngularJSRoute } from "../model";

export function generateRouteTable(routes: AngularJSRoute[]) {
  const table = new Table({
    head: ["Name", "Full url", "abstract", "Redirect", "controller"],
  });

  const routeData = routes.map((route) => [
    route.name,
    route.url,
    route.abstract,
    route.redirectToUrl !== undefined || route.redirectTo !== undefined,
    Array.isArray(route.controller) ? route.controller.join(',') : route.controller
  ]);
  table.push(...routeData);
  return table;
}
