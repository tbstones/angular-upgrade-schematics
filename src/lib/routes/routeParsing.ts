import { AngularJSRoute, UrlByNameDict } from "../model";

function countRoutes(routes: AngularJSRoute[]): number {
  return routes.length;
}

function routeNames(routes: AngularJSRoute[]): string[] {
  return routes.map((route: AngularJSRoute) => route.name);
}

function getRedirects(routes: AngularJSRoute[]): AngularJSRoute[] {
  return routes.filter((route: AngularJSRoute) => route.redirectToUrl);
}

function sortByRouteName(routes: AngularJSRoute[]): AngularJSRoute[] {
  return routes.sort(function (a, b) {
    var textA = a.name.toUpperCase();
    var textB = b.name.toUpperCase();
    return textA < textB ? -1 : textA > textB ? 1 : 0;
  });
}

function getUrlByNameDictionary(routes: AngularJSRoute[]): UrlByNameDict {
  return routes.reduce(
    (accum, value) => ({ ...accum, [value.name]: value.url }),
    {}
  );
}

function getFullRoute(routeName: string, nameUrlDictionary: UrlByNameDict) {
  return routeName
    .split(".")
    .reduce((accum: string[], value, index) => {
      const name = index === 0 ? value : `${accum[index - 1]}.${value}`;
      accum.push(name);
      return accum;
    }, [])
    .map((name: string) => nameUrlDictionary[name])
    .join("");
}

export function addFullRoutes(routes: AngularJSRoute[]): AngularJSRoute[] {
  const dict = getUrlByNameDictionary(routes);

  return sortByRouteName(routes).map((route: AngularJSRoute) => ({
    ...route,
    url: getFullRoute(route.name, dict),
  }));
}

// Returns a count for each root url segment 
export function getUrlRootCount(routes: AngularJSRoute[]) {
  return routes.reduce((accum: { [key: string]: number }, route) => {
    const root = route.url.split("/")[1];
    if (!accum.hasOwnProperty(root)) {
      accum[root] = 0;
    }
    accum[root] = accum[root] + 1;
    return accum;
  }, {});
}