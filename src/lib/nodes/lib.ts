import {
  ClassDeclaration, ConstructorDeclaration, FunctionDeclaration,
  FunctionExpression, PropertyAccessExpression, SyntaxKind
} from "typescript";
import { AngularTypes } from "../model/angular-types.model";
import { AstNode } from "../model/ast-node.model";

export function getNodesByKind<T>(nodes: any[], kind: SyntaxKind): T[] {
  return nodes.filter((n) => n.kind === kind) as T[];
}

export function getFirstNodeByKind<T>(
  nodes: AstNode[],
  kind: SyntaxKind
): T | undefined {
  return nodes.find((n) => n.kind === kind) as T | undefined;
}
export function getNodesByName<T>(nodes: any[], name: AngularTypes): T[] {
  return nodes.filter((n) => n?.name?.escapedText === name);
}

export function getFirstNodeByName<T>(
  nodes: any[],
  name: AngularTypes | string | number
): T | undefined {
  return nodes.find((n) => n?.name?.escapedText === name) as T | undefined;
}

export function getPropertyAccessExpressionNodes(
  flattened: AstNode[]
): PropertyAccessExpression[] {
  return getNodesByKind<PropertyAccessExpression>(
    flattened,
    SyntaxKind.PropertyAccessExpression
  );
}

export function getFirstFunctionDeclarationNode(
  astNodes: AstNode[]
): FunctionDeclaration | FunctionExpression | undefined {

  const declaration = getFirstNodeByKind<FunctionDeclaration>(
    astNodes,
    SyntaxKind.FunctionDeclaration
  );
  return (declaration) ? declaration
    : getFirstNodeByKind<FunctionExpression>(
      astNodes,
      SyntaxKind.FunctionExpression
    );

}

export function get$InjectNode(astNodes: AstNode[]): PropertyAccessExpression | undefined {
  return getFirstNodeByName<PropertyAccessExpression>(
    getPropertyAccessExpressionNodes(astNodes),
    "$inject"
  );
}

export function getFirstConstructorNode(astNodes: AstNode[]): ConstructorDeclaration | undefined {
  return getFirstNodeByKind<ConstructorDeclaration>(
    astNodes,
    SyntaxKind.Constructor
  );
}

export function getClassDeclarationNode(
  flattenedAst: AstNode[]
): ClassDeclaration | undefined {
  return getFirstNodeByKind<ClassDeclaration>(
    flattenedAst,
    SyntaxKind.ClassDeclaration
  );
}
