import { PropertyAssignment, SyntaxKind } from "typescript";
import { AstNode } from "../model/ast-node.model";
import { getFirstProperyAssignmentNodeByName } from "./get-first-property-assignment-node-by-name";

export function getControllerNode(flattenedAst: AstNode[]): PropertyAssignment | undefined {
    const node = getFirstProperyAssignmentNodeByName(flattenedAst, "controller");
    return (node?.initializer.kind === SyntaxKind.ArrayLiteralExpression) ? node : undefined;
}

