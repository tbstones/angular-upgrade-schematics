import { SourceFile } from "typescript";
import { AstNode } from "../model/ast-node.model";

export function getFlattenedSourceNodes(sourceFile: SourceFile): AstNode[] {
  const nodes: AstNode[] = [sourceFile];
  const result = [];

  while (nodes.length > 0) {
    const node = nodes.shift();

    if (node) {
      result.push(node);

      // if this node has children, append them to the nodes array.
      if (node.getChildCount(sourceFile) >= 0) {
        nodes.unshift(...node.getChildren());
      }
    }
  }

  return result;
}
