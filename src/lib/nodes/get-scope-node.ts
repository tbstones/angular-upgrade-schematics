import { PropertyAssignment } from "typescript";
import { AstNode } from "../model/ast-node.model";
import { getFirstProperyAssignmentNodeByName } from "./get-first-property-assignment-node-by-name";

export function getScopeBindingsNode(flattenedAst: AstNode[]): PropertyAssignment | undefined {
    return getFirstProperyAssignmentNodeByName(flattenedAst, "scope");
}
