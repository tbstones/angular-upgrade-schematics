import { PropertyAssignment } from "typescript";
import { AstNode } from "../model/ast-node.model";
import { getFirstProperyAssignmentNodeByName } from "./get-first-property-assignment-node-by-name";

export function getBindingsNode(flattenedAst: AstNode[]): PropertyAssignment | undefined {
    return getFirstProperyAssignmentNodeByName(flattenedAst, "bindings");
}

