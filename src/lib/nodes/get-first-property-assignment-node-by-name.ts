import { PropertyAssignment, SyntaxKind } from "typescript";
import { getFirstNodeByName, getNodesByKind } from ".";
import { AstNode } from "../model/ast-node.model";

export function getFirstProperyAssignmentNodeByName(flattenedAst: AstNode[], name: string): PropertyAssignment | undefined {
    return getFirstNodeByName<PropertyAssignment>(
        getNodesByKind(flattenedAst, SyntaxKind.PropertyAssignment),
        name
    );
}
