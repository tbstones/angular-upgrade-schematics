import { readFileSync } from "fs";
import * as ts from "typescript";

export function getSourceFile(file: string): ts.SourceFile {
  return ts.createSourceFile(
    file,
    readFileSync(file).toString(),
    ts.ScriptTarget.Latest,
    true
  );
}