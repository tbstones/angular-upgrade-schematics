export enum AngularTypes {
  Controller = "controller",
  Service = "service",
  Factory = "factory",
  Component = "component",
  Directive = "directive",
}