import { Bindings } from "../bindings/model";
import { InjectionPropsToTokens } from "../di";
export * from './route';

export interface ClassVariable{
    name: string;
    initialiser?: string;
}
export interface ServiceTemplateData {
  originalFile: string;
  className?: string;
  classProperties?: ClassVariable[];
  classMethods?: ClassMethod[];
  constructorStatements?: string[];
  dependencies?: InjectionPropsToTokens;
  fileName?: string;
}

export interface ComponentTemplateData extends ServiceTemplateData{
  selector?: string;
  inputs?: string[];
  bindings?: Bindings;
  lifeCycle : string[];
}

export interface ClassMethod {
  name?: string;
  params?: string[];
  body?: string[];
}