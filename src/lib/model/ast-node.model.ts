
import * as ts from "typescript";

export type AstNode  = ts.MethodDeclaration | ts.Node | ts.PropertyAccessExpression;