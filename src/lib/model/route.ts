export interface AngularJSRoute {
  name: string;
  url: string;
  views?: null;
  abstract?: boolean;
  redirectToUrl?: string;
  template?: string;
  controller?: Array<null | string> | string;
  resolve?: { [key: string]: Array<string | null> };
  data?: { [key: string]: string | boolean };
  controllerAs?: string;
  redirectTo?: string;
  params?: { [key: string]: null | boolean };
}

export interface UrlByNameDict {
  [key: string]: string;
}
