import { Rule, SchematicContext, Tree } from "@angular-devkit/schematics";
import { readFileSync, writeFileSync } from "fs";
import { json2csv } from 'json-2-csv';
import { AngularJSRoute } from "../lib/model/route";
import { addFullRoutes } from "../lib/routes";

export function processRoutes(_options: any): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    try {
      const appRoutes: AngularJSRoute[] = JSON.parse(
        readFileSync(`${_options.path}`, "utf8")
      );

      const fullRoutes = addFullRoutes(appRoutes);

      const routeData = fullRoutes
      .filter((route)=> !route.abstract || (route.redirectToUrl !== undefined || route.redirectTo !== undefined))
      .map((route) => ({
        Name: route.name,
        "Full url" : route.url,
        Status: 'Not converted',
      }));

      json2csv(routeData, (err, csv) => {
        if(!err){
          writeFileSync(`routes.csv`, csv);
        }else{
          console.log('There was an error creating the CSV')
        }
      });

      // console.log(generateRouteTable(fullRoutes).toString());
    } catch (error) {
      console.log(error);
    }

    return tree;
  };
}
