# Schematics

> This makes sense to my brain now.

## Installation

First off, install the schematics dev tools: `npm install -g @angular-devkit/schematics-cli`.

Install dependencies (`npm install`) and then run `npm link`. Now you can run call the schematics from any folder.

## Development

To develop, run the `npm run build:watch` command, and then run the schematic command you want. This will rebuild when you make changes to commands

## Convert a component

command :`schematics angularjs-converter:c --path <fileName>`

Converts a angularJS component into Angular component.

> IMPORTANT: the controller needs to be in the same file as the component for conversion to work properly


## Convert a directive component

command :`schematics angularjs-converter:dc --path <fileName>`

Converts a angularJS directive component into Angular component.

> IMPORTANT: the controller needs to be in the same file as the directive component for conversion to work properly

## Convert a template

command :`schematics angularjs-converter:t --path <fileName>`

Converts a angularJS template html into Angular template.


## Convert a directive

command :`schematics angularjs-converter:d --path <fileName>`

Converts a angularJS directive into Angular directive. This isn't very good at the moment, but we also don't have many.

> IMPORTANT: the controller needs to be in the same file as the directive for conversion to work properly


## Process service

command :`schematics angularjs-converter:process-service`

Converts a angularJS service into Angular services.

> IMPORTANT: This doesn't handle factories very well. Try to convert the factory js to a service first.


## Process routes

command: `schematics angularjs-converter:process-route --path path/to/routes.json`

### Preparing the `route.json` file

To get the route structure of the routes from the existing angular routes, somewhere in you applcation add `console.log(JSON.stringify(this.$state.get()));`.

## Gotchas

### Components

#### Single file

Some components have the controller in a separate file, you will need to copy the controller ( including `$inject` if present ) into the component/directive file underneath the directive/component declaration

#### Component has no controller

Some simple components/directives contain no logic, so no controller is supplied. To get the schematic to work on theses files, you need to 
Add `controller: [() => {}],` to the declaration.

``` javascript
(() => {
  angular.module('rmaDashboard').component('rmaDashboardReviewEmailStatus', {
    template,
    bindings: {
      reviewRequest: '<'
    },
    controller: [() => {}],
  });
})();
```
